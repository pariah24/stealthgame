﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridNavigation : MonoBehaviour
{
    public class GridPath
    {
        public List<GridObject.GridCoordinate> PathCoordinates = new List<GridObject.GridCoordinate>();
    }

    int[][] walkableFlags;
    int gridSize;
    GridManager gridMan;



    public void Init(int gridSize)
    {
        this.gridSize = gridSize;
        gridMan = GetComponent<GridManager>();

        //create array which hold flags for while tiles can be moved through
        walkableFlags = new int[gridSize][];
        for (int x = 0; x < gridSize; x++)
            walkableFlags[x] = new int[gridSize];

        //request flags to be set
        gridMan.SetNavFlags(walkableFlags);
    }


    public GridPath GetPathForObjectToSpace(GridObject gridObj, Vector3 worldPos)
    {
        bool appendNode = false;
        bool insertNode= false;

        GridObject.GridCoordinate appendPoint= gridObj.coordinates;
        GridObject.GridCoordinate insertPoint = gridObj.coordinates;
        GridObject.GridCoordinate endPos = gridMan.worldToGrid(worldPos);
        GridObject.GridCoordinate startPos = gridObj.coordinates;

        //see if the player is trying to leave from or to victory pillar(not part of the grid)
        if (gridObj.canBePlacedVictoryTiles && endPos != startPos)
        {
            foreach (var pillar in gridMan.victoryPillars)
            {
                if (pillar.isActivated )
                {
                    if (pillar.offGrid == endPos)
                    {
                        appendNode = true;
                        endPos = pillar.onGrid;
                        appendPoint = pillar.offGrid;
                    }
                    if (pillar.offGrid == startPos)
                    {
                        insertNode = true;
                        startPos = pillar.onGrid;
                        insertPoint = pillar.offGrid;
                    }
                }
            }
        }

        //put coordinates into arrays for astar to use
        int[] start = new int[2] { startPos.x, startPos.y };
        int[] end = new int[2] { endPos.x, endPos.y };


        //if click is outside bounds of the grid, return null since no path can be made
        if (endPos.x < 0 || endPos.y < 0 || endPos.x >= gridSize || endPos.y >= gridSize)
            return null;

        //return no path, this is not somewhere the object can move to
        if (walkableFlags[endPos.y][endPos.x] == 1)
            return null;

        List<Vector2> gridPath = new Astar(walkableFlags, start, end, "").result;
        GridPath path = new GridPath();

        //make list of coordinates to move to.
        for (int i = 0; i < gridPath.Count; i++)
        {
            int x = (int)gridPath[i].x;
            int y = (int)gridPath[i].y;
            path.PathCoordinates.Add(new GridObject.GridCoordinate(x, y, gridMan.GridToWorld(x, y)));
        }

        //if legal path, insert/append spots for the pillars
        if (path.PathCoordinates.Count > 0)
        {
            if (appendNode)
                path.PathCoordinates.Add(appendPoint);
            if (insertNode)
                path.PathCoordinates.Insert(0, insertPoint);
        }

        return path;
    }

    public GridObject.GridCoordinate GetGridCoordinate(Vector3 pos)
    {
        return gridMan.worldToGrid(pos);
    }

    public void UpdateObjectGridPosition(GridObject newGribObj)
    {
        newGribObj.coordinates = gridMan.worldToGrid(newGribObj.transform.localPosition);
    }
}
