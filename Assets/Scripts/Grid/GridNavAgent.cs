﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridNavAgent : MonoBehaviour
{
    GridNavigation gridNav;
    Character character;

    float rotationSpeed = 1;
    float movementSpeed = 1;
    bool canRotateWhileMoving;

    GridNavigation.GridPath PathToMoveAlong;   
    public Vector3 lastPos;
    public Vector3 targetPos;
    Quaternion lastRotation;
    Quaternion rotationTarget;
    float timeMoving;
    float timeRotating;

    public void Init(GridNavigation gridNav, float movementSpeed, float rotationSpeed, bool canRotateWhileMoving)
    {
        this.canRotateWhileMoving = canRotateWhileMoving;
        this.movementSpeed = movementSpeed;
        this.rotationSpeed = rotationSpeed;
        this.gridNav = gridNav;
        character = GetComponent<Character>();
        lastPos = character.GetWorldPosition();
        lastRotation = transform.rotation;
    }

    //return if path to location exists and the grid space for it
    public bool RequestMove(Vector3 worldPos, out GridObject.GridCoordinate spacePressed)
    {
        spacePressed = gridNav.GetGridCoordinate(worldPos);
        return RequestMove(worldPos);
    }

    //return if a path to location exists
    public bool RequestMove(Vector3 worldPos)
    {
        GridObject.GridCoordinate pressedOnLocation = gridNav.GetGridCoordinate(worldPos);

        //dont make new path if we are already on a path that has the same final desintaion
        if (PathToMoveAlong != null && PathToMoveAlong.PathCoordinates.Count > 0)
        {
            if(pressedOnLocation == PathToMoveAlong.PathCoordinates[PathToMoveAlong.PathCoordinates.Count - 1])
            {
                return true;
            }
        }

        //ask grid navigation for path to position
        GridNavigation.GridPath newPath = gridNav.GetPathForObjectToSpace(character, worldPos);

        //if no path to spot exists, do not interrupt the current path we are following
        if (newPath != null)
        {
            // if the next space we will travel to is in the same direction we are already traveling, remove first entry because we dont want to double back
            if (newPath.PathCoordinates.Count > 1 && targetPos == newPath.PathCoordinates[1].worldPosition)
                newPath.PathCoordinates.RemoveAt(0);
            //if the second node is directly behind us, get rid of first node since we can just rotate backwards and move to second node. has to be 180 otherwise the player could travel diagonally
            else if (newPath.PathCoordinates.Count > 1 && Vector3.Angle(character.GetWorldPosition() - newPath.PathCoordinates[1].worldPosition, character.GetWorldPosition() - targetPos) == 180)
                newPath.PathCoordinates.RemoveAt(0);


            //set new path
            PathToMoveAlong = newPath;
            SetNextDestination();

            return true;
        }
        else
        {
            return false;
        }
    }

    void Update()
    {
        if (!character.isAlive)
            return;

        if (PathToMoveAlong != null)
        {
            timeRotating += (Time.deltaTime * rotationSpeed) / Quaternion.Angle(rotationTarget, lastRotation);
            transform.rotation = Quaternion.Slerp(lastRotation, rotationTarget, timeRotating / 1);

            if (canRotateWhileMoving || timeRotating >= 1)
            {
                //move ahead time and move character. interpolation rate is relative to distance that needs to be covered to maintin constant speed
                timeMoving += Time.deltaTime * movementSpeed / (Vector3.Distance(lastPos, targetPos));
                character.SetWorldPosition(Vector3.Lerp(lastPos, targetPos, timeMoving));
            }

            //if greater then 1 we are in the space we were travelling to, remove and start moving to next space
            if (timeMoving >= 1)
            {
                SetNextDestination();
            }
            
        }

        //update our grid position in case we moved from one space into another
        gridNav.UpdateObjectGridPosition(character);
    }

    void SetNextDestination()
    {
        //if we have moved to every point on path, return and set path to null
        if (PathToMoveAlong.PathCoordinates.Count == 0)
        {
            PathToMoveAlong = null;
            return;
        }

        //set new targets
        targetPos = PathToMoveAlong.PathCoordinates[0].worldPosition;

        if (targetPos == transform.localPosition)
            rotationTarget = lastRotation;
        else if(targetPos != character.GetWorldPosition()) // get rid of unity warning for zero look rotation
            rotationTarget = Quaternion.LookRotation(targetPos - character.GetWorldPosition());
        
        //reset
        timeMoving = 0;
        timeRotating = 0;
        lastPos = character.GetWorldPosition();
        lastRotation = transform.rotation;

        //remove point form path since were are travelling there next
        PathToMoveAlong.PathCoordinates.RemoveAt(0);
    }
}
