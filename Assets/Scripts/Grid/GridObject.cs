﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject : MonoBehaviour
{
    public enum Direction { UP, LEFT, DOWN, RIGHT }

    public struct GridCoordinate
    {
        public int x, y;
        public Vector3 worldPosition;

        public GridCoordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
            worldPosition = Vector3.zero;
        }

        public GridCoordinate(int x, int y, Vector3 worldPos)
        {
            this.x = x;
            this.y = y;
            worldPosition = worldPos;
        }

        public static bool operator == (GridCoordinate c1, GridCoordinate c2)
        {
            return c1.x == c2.x && c1.y == c2.y;
        }

        public static bool operator !=(GridCoordinate c1, GridCoordinate c2)
        {
            return c1.x != c2.x || c1.y != c2.y;
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode();
        }

        public override bool Equals(object o)
        {
            return (o.GetType() == typeof(GridCoordinate)) && ((GridCoordinate)o == this);
        }

        public static GridCoordinate operator +(GridCoordinate c1, GridCoordinate c2)
        {
            c1.x = c1.x + c2.x;
            c1.y = c1.y + c2.y;
            return c1;
        }

        public static GridCoordinate operator -(GridCoordinate c1, GridCoordinate c2)
        {
            c1.x = c1.x - c2.x;
            c1.y = c1.y - c2.y;
            return c1;
        }
    }


    public static Direction GetOppositeDirection(Direction d)
    {
        return (Direction)(((int)d + 2) % 4);
    }

    public static GridCoordinate GetOffset(Direction d)
    {
        switch(d)
        {
            case Direction.UP: return new GridCoordinate(0, 1);
            case Direction.DOWN: return new GridCoordinate(0, -1);
            case Direction.LEFT: return new GridCoordinate(-1, 0);
            case Direction.RIGHT: return new GridCoordinate(1, 0);
            default:
                Debug.LogError("GridObject.GridCoordinate(): given a invalid direction");
                return new GridCoordinate(0, 0);
        }
    }

    static float centerPivotOffset = 0.5f;

    [HideInInspector]
    public bool placedInGrid;
    public GridCoordinate coordinates = new GridCoordinate(-1, -1, Vector3.zero);
    public bool canBePlacedVictoryTiles = false;
    public bool centerPivot;


    public virtual void SetWorldPosition(Vector3 worldPosition)
    {
        //offset if the pivot of object is in the center
        if(centerPivot)
            worldPosition.y = centerPivotOffset;

        transform.localPosition = worldPosition;
    }

    public virtual Vector3 GetWorldPosition()
    {
        Vector3 pos = transform.localPosition;

        if (centerPivot)
            pos.y = 0;

        return pos;
    }
}
