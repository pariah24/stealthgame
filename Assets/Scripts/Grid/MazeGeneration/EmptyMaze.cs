﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EmptyMazeType", menuName = "Maze/Empty", order = 1)]
public class EmptyMaze : MazeGenerationType
{
    public override MazeLayout GenerateLayout(int gridSize, VictoryPillar startPillar)
    {
        MazeLayout layout = new MazeLayout();

        layout.playerStartPosOnGrid = new GridObject.GridCoordinate(0, 0);
        layout.playerStartPosOffGrid = new GridObject.GridCoordinate(0, -1);
        layout.playerEndPosOnGrid = new GridObject.GridCoordinate(gridSize - 1, gridSize - 1);
        layout.playerEndPosOffGrid = new GridObject.GridCoordinate(gridSize - 1, gridSize);

        return layout;
    }
}
