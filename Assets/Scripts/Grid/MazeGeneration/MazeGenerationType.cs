﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MazeGenerationType : ScriptableObject
{
    public class MazeLayout
    {
        public GridObject.GridCoordinate playerStartPosOnGrid;
        public GridObject.GridCoordinate playerStartPosOffGrid;
        public GridObject.GridCoordinate playerEndPosOnGrid; // the spot adjacent to victory tile
        public GridObject.GridCoordinate playerEndPosOffGrid; // the actual victory tile the lies off of the true grid

        public List<GridObject.GridCoordinate> wallSpaces = new List<GridObject.GridCoordinate>();
        public List<GridObject.GridCoordinate> pickupLocations = new List<GridObject.GridCoordinate>();
        public List<List<GridObject.GridCoordinate>> enemyPatrolPaths = new List<List<GridObject.GridCoordinate>>();
    }

    public abstract MazeLayout GenerateLayout(int gridSize, VictoryPillar startPillar);

    public int gridSize = 11;
    public int numberOfEnemies = 1;
}
