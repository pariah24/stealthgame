﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColumnMaze", menuName = "Maze/Column", order = 1)]
public class ColumnMaze : MazeGenerationType
{
    const int patrolPointsPerPath = 3;

    public int pickupsToWin = 1;
    public int numberOfRandomWalls = 0;

    public override MazeLayout GenerateLayout(int gridSize, VictoryPillar startPillar)
    {
        MazeLayout layout = new MazeLayout();

        //first level, place start pillar up
        if (startPillar == null)
        {
            layout.playerStartPosOnGrid = GetRandomOddExteriorCoordinate(GridObject.Direction.UP, gridSize);
            layout.playerStartPosOffGrid = new GridObject.GridCoordinate(layout.playerStartPosOnGrid.x, layout.playerStartPosOnGrid.y + 1);
        }
        else //if second level or later, use start position given to us from previous level
        {
            layout.playerStartPosOnGrid = startPillar.onGrid;
            layout.playerStartPosOffGrid = startPillar.offGrid;
        }

        //place end pillar on side different to start
        GridObject.Direction differentSide = GetDifferentDirection(GetDirectionFromWallCorrdinate(layout.playerStartPosOnGrid, gridSize));
        layout.playerEndPosOnGrid = GetRandomOddExteriorCoordinate(differentSide, gridSize);
        layout.playerEndPosOffGrid = layout.playerEndPosOnGrid + GridObject.GetOffset(differentSide);


        //make temporary array for checking pathfinding. 0 = walkable, 1 = blocked. astar library expects coordinates in form y,x
        int[] start = new int[2] { layout.playerStartPosOnGrid.x, layout.playerStartPosOnGrid.y };
        int[] end = new int[2] { layout.playerEndPosOnGrid.x, layout.playerEndPosOnGrid.y };
        int[][] walkableFlags = new int[gridSize][];
        for (int y = 0; y < gridSize; y++)
        {
            walkableFlags[y] = new int[gridSize];
            for (int x = 0; x < gridSize; x++)
                walkableFlags[y][x] = 0;
        }

        //keep track of number of random walls added and pickup locations
        List<GridObject.GridCoordinate> walkableLocations = new List<GridObject.GridCoordinate>();
        List<GridObject.GridCoordinate> randomWalls = new List<GridObject.GridCoordinate>();

        //place all column walls
        for (int x = 0; x < gridSize; x++)
        {
            for (int y = 0; y < gridSize; y++)
            {
                if (x % 2 == 0 && y % 2 == 0) //uniform column
                {
                    walkableFlags[y][x] = 1;
                    layout.wallSpaces.Add(new GridObject.GridCoordinate(x, y));
                }
                else if((x != layout.playerStartPosOnGrid.x || y != layout.playerStartPosOnGrid.y) && (x != layout.playerEndPosOnGrid.x || y != layout.playerEndPosOnGrid.y)) // if not start or end, add to possible walkablespots
                {
                    randomWalls.Add(new GridObject.GridCoordinate(x, y));
                    walkableLocations.Add(new GridObject.GridCoordinate(x, y));
                }
            }
        }

        //go through available spots and place random walls
        int randomWallsPlaced = 0;
        while(randomWallsPlaced < numberOfRandomWalls && randomWalls.Count > 0)
        { 
            int randomIndex = Random.Range(0, randomWalls.Count - 1);
            if(isWallValid(walkableFlags, start, end, randomWalls[randomIndex].x, randomWalls[randomIndex].y))
            {
                walkableFlags[randomWalls[randomIndex].y][randomWalls[randomIndex].x] = 1;
                layout.wallSpaces.Add(new GridObject.GridCoordinate(randomWalls[randomIndex].x, randomWalls[randomIndex].y));
                randomWallsPlaced++;
                walkableLocations.Remove(randomWalls[randomIndex]);
            }

            //dont revist
            randomWalls.RemoveAt(randomIndex);
        }

        //remove spots that are no longer reachable
        for (int i = 0; i < walkableLocations.Count; i++)
        {
            if(!IsReachable(walkableFlags, start, walkableLocations[i].x, walkableLocations[i].y))
            {
                walkableLocations.RemoveAt(i);
                i--;
            }
        }

        //place enemy patrol points. enemies always move between 3 points
        while(layout.enemyPatrolPaths.Count < numberOfEnemies && walkableLocations.Count >= patrolPointsPerPath)
        {
            List<GridObject.GridCoordinate> patrolPoints = new List<GridObject.GridCoordinate>();
            while(patrolPoints.Count < patrolPointsPerPath && walkableLocations.Count >= patrolPointsPerPath)
            {
                int randomIndex = Random.Range(0, walkableLocations.Count - 1);
                if (!patrolPoints.Contains(walkableLocations[randomIndex]))
                    patrolPoints.Add(walkableLocations[randomIndex]);
            }

            layout.enemyPatrolPaths.Add(patrolPoints);
        }

        //add pickups
        int placedPickups = 0;
        while (placedPickups < pickupsToWin && walkableLocations.Count > 0)
        {
            int randomI = Random.Range(0, walkableLocations.Count - 1);
            layout.pickupLocations.Add(walkableLocations[randomI]);
            walkableLocations.RemoveAt(randomI);
            placedPickups++;
        }

        return layout;
    }

    bool isWallValid(int[][] walkableFlags, int[] start, int[] end, int x, int y)
    {
        //pretend x, y is a wall and see if we can still reach destination, then revert
        walkableFlags[y][x] = 1;
        List<Vector2> gridPath = new Astar(walkableFlags, start, end, "").result;
        walkableFlags[y][x] = 0;

        return gridPath != null && gridPath.Count > 0;
    }

    bool IsReachable(int[][] walkableFlags, int[] start,  int x, int y)
    {
        List<Vector2> gridPath = new Astar(walkableFlags, start, new int[] {x, y}, "").result;

        return gridPath != null && gridPath.Count > 0;
    }

    GridObject.Direction GetDifferentDirection(GridObject.Direction initial)
    {
        return (GridObject.Direction)(((int)initial + Random.Range(1, 4)) % 4); // add between 1 - 3 to current direction
    }

    GridObject.Direction GetDirectionFromWallCorrdinate(GridObject.GridCoordinate coordinates, int gridSize)
    {
        if (coordinates.y == gridSize - 1)
            return GridObject.Direction.UP;
        else if (coordinates.y == 0)
            return GridObject.Direction.DOWN;
        else if (coordinates.x == gridSize - 1)
            return GridObject.Direction.RIGHT;
        else if (coordinates.x == 0)
            return GridObject.Direction.LEFT;
        else
            Debug.LogError("UniformMaze.GetDirectionFromWallCorrdinate(): given a invalid direction");
            return GridObject.Direction.UP;// shouldnt get here
    }

    GridObject.GridCoordinate GetRandomOddExteriorCoordinate(GridObject.Direction directionOfSide, int gridSize)
    {
        switch (directionOfSide)
        {
            case GridObject.Direction.UP:
                return new GridObject.GridCoordinate((Random.Range(0, gridSize / 2) * 2) + 1, gridSize - 1);
            case GridObject.Direction.DOWN:
                return new GridObject.GridCoordinate((Random.Range(0, gridSize / 2) * 2) + 1, 0);
            case GridObject.Direction.LEFT:
                return new GridObject.GridCoordinate(0, (Random.Range(0, gridSize / 2) * 2) + 1);
            case GridObject.Direction.RIGHT:
                return new GridObject.GridCoordinate(gridSize - 1, (Random.Range(0, gridSize / 2) * 2) + 1);
            default:
                Debug.LogError("UniformMaze.GetRandomOddExteriorCoordinate(): given a invalid direction");
                return new GridObject.GridCoordinate(0, 0); // shouldnt arrive here
        }
    }
}
