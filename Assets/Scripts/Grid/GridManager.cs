﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GridNavigation))]
public class GridManager : MonoBehaviour
{
    class GridSpace
    {
        GridObject occupyingObj;
        public GridObject.GridCoordinate coordinates;      

        public GridSpace(GridObject.GridCoordinate gridCoordinates)
        {
            coordinates = gridCoordinates;
            occupyingObj = null;
        }

        public bool Passable()
        { 
            return occupyingObj == null;
        }

        public void PlaceObject(GridObject go, bool occupy)
        {
            if(occupy)
                occupyingObj = go;

            go.placedInGrid = true;
            go.coordinates = coordinates;
        }

        public void DestroyOccupyingObject()
        {
            if(occupyingObj)
                Destroy(occupyingObj.gameObject);
        }
    }

    public GameObject groundObject;
    public VictoryPillar victoryPillarPrefab;
    public GridObject wallPrefab;
    public GridNavigation gridNav;

    [HideInInspector]
    public VictoryPillar startPillar;
    [HideInInspector]
    public VictoryPillar endPillar;
    [HideInInspector]
    public List<VictoryPillar> victoryPillars;


    int gridSize;
    float gridToWorldOffset;
    GridSpace[] gridSpaces;
    MazeGenerationType.MazeLayout currentLayout;


    public void Init(int gridSize, MazeGenerationType.MazeLayout mazeLayout, VictoryPillar previousEndPillar)
    {
        victoryPillars = new List<VictoryPillar>();
        this.gridSize = gridSize;
        gridSpaces = new GridSpace[gridSize * gridSize];
        gridToWorldOffset = -(gridSize - 1) / 2f;

        groundObject.transform.localScale = new Vector3(gridSize/10f, 1, gridSize/10f);

        //create grid and add walls
        for (int x = 0; x < gridSize ; x++)
        {
            for (int y = 0; y < gridSize; y++)
            {
                gridSpaces[GridToIndex(x, y)] = new GridSpace(new GridObject.GridCoordinate(x, y, GridToWorld(x, y)));
            }
        }

        //places walls according to layout
        foreach (var wall in mazeLayout.wallSpaces)
        {
            GridObject wallObj = Instantiate(wallPrefab, transform);
            wallObj.SetWorldPosition(GridToWorld(wall.x, wall.y));
            gridSpaces[wall.x + wall.y * gridSize].PlaceObject(wallObj, true);
        }


        //create victory pillar for end
        GridObject.GridCoordinate onGridCoordinates = new GridObject.GridCoordinate(mazeLayout.playerEndPosOnGrid.x, mazeLayout.playerEndPosOnGrid.y, GridToWorld(mazeLayout.playerEndPosOnGrid.x, mazeLayout.playerEndPosOnGrid.y));
        GridObject.GridCoordinate offGridCoordinates = new GridObject.GridCoordinate(mazeLayout.playerEndPosOffGrid.x, mazeLayout.playerEndPosOffGrid.y, GridToWorld(mazeLayout.playerEndPosOffGrid.x, mazeLayout.playerEndPosOffGrid.y));
        endPillar = Instantiate(victoryPillarPrefab, GridToWorld(offGridCoordinates.x, offGridCoordinates.y), Quaternion.identity);
        endPillar.Init(true, onGridCoordinates, offGridCoordinates);

        //create victory pillar for start if we werent given an existing one
        if (previousEndPillar != null)
        {
            startPillar = previousEndPillar;
            startPillar.Init(false, startPillar.onGrid, startPillar.offGrid);
        }
        else
        {
            onGridCoordinates = new GridObject.GridCoordinate(mazeLayout.playerStartPosOnGrid.x, mazeLayout.playerStartPosOnGrid.y, GridToWorld(mazeLayout.playerStartPosOnGrid.x, mazeLayout.playerStartPosOnGrid.y));
            offGridCoordinates = new GridObject.GridCoordinate(mazeLayout.playerStartPosOffGrid.x, mazeLayout.playerStartPosOffGrid.y, GridToWorld(mazeLayout.playerStartPosOffGrid.x, mazeLayout.playerStartPosOffGrid.y));
            startPillar = Instantiate(victoryPillarPrefab, GridToWorld(offGridCoordinates.x, offGridCoordinates.y), Quaternion.identity);
            startPillar.Init(false, onGridCoordinates, offGridCoordinates);
        }


        victoryPillars.Add(startPillar);
        victoryPillars.Add(endPillar);

        //init navigation for pathfinding
        gridNav.Init(gridSize); 
    }

    public void DestroyGridContents(bool destroyEndPillar = true)
    {
        //remove everything in grid
        if(gridSpaces != null)
            foreach (var space in gridSpaces)
                space.DestroyOccupyingObject();

        gridSpaces = new GridSpace[1];

        if (destroyEndPillar && endPillar != null)
            Destroy(endPillar.gameObject);
        if (startPillar != null)
            Destroy(startPillar.gameObject);          
    }

    public Vector3 GridToWorld(int x, int y)
    {
        return new Vector3(x + gridToWorldOffset, 0, y + gridToWorldOffset);
    }

    public GridObject.GridCoordinate worldToGrid(Vector3 worldPos)
    {
        int x = Mathf.RoundToInt(worldPos.x - gridToWorldOffset);
        int y = Mathf.RoundToInt(worldPos.z - gridToWorldOffset);

        return new GridObject.GridCoordinate(x, y, GridToWorld(x, y));
    }

    int GridToIndex(int x, int y)
    {
        return x + y * gridSize;
    }

    GridObject.GridCoordinate IndexToGrid(int i)
    {
        int x = i % gridSize;
        int y = i / gridSize;

        return new GridObject.GridCoordinate(x, y, GridToWorld(x, y));
    }

    public void SetNavFlags(int[][] flags)
    {
        for (int y = 0; y < gridSize; y++)
            for (int x = 0; x < gridSize; x++)
                flags[y][x] = gridSpaces[GridToIndex(x, y)].Passable() ? 0 : 1;
    }

    public void PlaceObjectOnRandomSpace(GridObject gridObj)
    {
        GridSpace openSpace = GetRandomVacantSpace();

        // if open spot, assign object to it and place it on the correct world location for it
        if (openSpace != null)
        {
            PlaceObjectOnSpace(gridObj, openSpace.coordinates.worldPosition);
        }
    }

    public void PlaceObjectOnSpace(GridObject gridObj, Vector3 point)
    {
        GridObject.GridCoordinate coordinates = worldToGrid(point);
        int gridIndex = GridToIndex(coordinates.x, coordinates.y);

        // if open spot, assign object to it
        if (gridIndex >= 0 && gridIndex < gridSize * gridSize)
        {
            gridSpaces[gridIndex].PlaceObject(gridObj, false);        
        }

        //place in world
        gridObj.SetWorldPosition(GridToWorld(coordinates.x, coordinates.y));
    }

    public bool GetRandomVacantPoint(out Vector3 point)
    {
        GridSpace openSpace = GetRandomVacantSpace();

        //set point if we found one and return if a vacant spot exists
        if (openSpace != null)
        {
            point = openSpace.coordinates.worldPosition;
            return true;
        }
        else
        {
            point = Vector3.zero;
            return false;
        }
    }

    GridSpace GetRandomVacantSpace()
    {
        GridSpace openSpace = null;
        List<GridSpace> checkedSpaces = new List<GridSpace>();

        //check random spaces until we find vacant one or checked every space
        do
        {
            int randomIndex = Random.Range(0, gridSpaces.Length);
            openSpace = gridSpaces[randomIndex];
            checkedSpaces.Add(gridSpaces[randomIndex]);           
        } while (checkedSpaces.Count < gridSpaces.Length && !openSpace.Passable());

        //return space if its passable, else null
        if (openSpace.Passable())
            return openSpace;
        else
            return null;
    }
}
