﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    public static SoundEffects instance = null;

    AudioSource source;

	void Start ()
    {
        source = GetComponent<AudioSource>();

        //set instance and make sure only 1 exists
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        
        DontDestroyOnLoad(gameObject);
    }

    public void PlayClip(AudioClip clip)
    {
        source.PlayOneShot(clip);
    }

    public void PlayClipWithDelay(AudioClip clip, float delay)
    {
        StartCoroutine(DelayPlay(clip, delay));
    }

    IEnumerator DelayPlay(AudioClip clip, float delay)
    {
        yield return new WaitForSeconds(delay);

        PlayClip(clip);
    }
}
