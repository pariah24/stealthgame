﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
   
    public float visibilityAngle = 90;  //angle of sight radius
    public float visbilityDistance = 1; //distance away player can be before being seen
    public float maxChaseDuration = 2;

    public float PlayerVisibilty { get; private set; } // 0 is no visibility, 1 is fully visibly and detected
    public bool inChase { get; private set; }

    bool movingForwardInPatrol;
    List<Vector3> patrolPath;
    public int currentNodeIndex;
    int raycastLayerMask;
    Player playerInLevel;
    float timeChasingPlayer;

    public void Init(GameMode gMode, List<Vector3> patrolPath, Player playerInLevel)
    {
        base.Init(gMode);

        this.patrolPath = patrolPath;
        this.playerInLevel = playerInLevel;
        movingForwardInPatrol = false;

        //create mask
        raycastLayerMask = LayerMask.GetMask("Walls", "Default");

        //if less then 1 dont patrol since its essentially idle. shouldnt get here though
        if (patrolPath.Count > 1)
        {
            currentNodeIndex = 0;
            gridNavAgent.RequestMove(patrolPath[0]);
        }

        //modify size of spot light based of visibilty radius
        GetComponentInChildren<Light>().spotAngle = visibilityAngle;
        GetComponentInChildren<Light>().range = visbilityDistance;
    }

	void Update()
    {
        //if player is alive
        if (playerInLevel.isAlive)
        {
            //clear previous visibility from last frame
            PlayerVisibilty = 0;

            //raycast for player visibility if within sight radius
            float angleToPlayerDir = Vector3.Angle(transform.forward, playerInLevel.transform.position - transform.position);
            if (visibilityAngle / 2.0f >= angleToPlayerDir)
            {

                //cast to see if we hit player
                RaycastHit hit;
                if (Physics.Raycast(transform.position, playerInLevel.transform.position - transform.position, out hit, Mathf.Infinity, raycastLayerMask) && hit.transform == playerInLevel.transform)
                {
                    //set distanceToPlayer based of visibilty
                    float distanceToPlayer = Vector3.Distance(hit.point, transform.position);
                    if (distanceToPlayer <= visbilityDistance / 2f)
                        PlayerVisibilty = 1;
                    else
                        PlayerVisibilty = 1f - Mathf.Clamp((distanceToPlayer - (visbilityDistance / 2f)) / (visbilityDistance), 0, 1);
                }
            }

            //reset chase timer since we see player
            if (PlayerVisibilty >= 0.95f)
                timeChasingPlayer = 0;


            //chase player if fully visible or we saw them recently
            if (PlayerVisibilty >= 0.95f || (inChase && timeChasingPlayer <= maxChaseDuration))
            {
                //if player is not visible but we are still chasing, set visibilty to show they are "losing threat"
                if (inChase && PlayerVisibilty == 0)
                    PlayerVisibilty = 1 - (timeChasingPlayer / maxChaseDuration);

                inChase = true;
                timeChasingPlayer += Time.deltaTime;
            }
            else if (inChase)
            {
                inChase = false;
                timeChasingPlayer = 0;

                //go back to patrol path if were done chasing
                gridNavAgent.RequestMove(patrolPath[currentNodeIndex]);
            }
        }
        else if(inChase)
        {
            //player died, just go back to patroling
            inChase = false;
            gridNavAgent.RequestMove(patrolPath[currentNodeIndex]);
        }
        
        if (inChase) //if we are in persuit, follow player
        {
            gridNavAgent.RequestMove(playerInLevel.transform.localPosition);
        }
        else if (patrolPath.Count > 0 && Vector3.Distance(patrolPath[currentNodeIndex], GetWorldPosition()) < 0.01f)   //if we have patrol points, move to each in order and then loop
        {
            //flip direction at ends of patrol path
            if (currentNodeIndex == 0 || currentNodeIndex == patrolPath.Count - 1)
                movingForwardInPatrol = !movingForwardInPatrol;

            //move to next node in patrol
            if (movingForwardInPatrol)
                currentNodeIndex++;
            else
                currentNodeIndex--;


            gridNavAgent.RequestMove(patrolPath[currentNodeIndex]);
        }
    }


    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.GetComponent<Player>() != null)
        {
            gameMode.KillPlayer();
        }
    }
}
