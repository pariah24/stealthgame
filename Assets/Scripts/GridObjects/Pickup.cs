﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : GridObject
{
    public AudioClip pickupSound;

    GameMode gameMode;
    bool consumed;

    public void Init(GameMode gameMode, Vector3 worldPos)
    {
        this.gameMode = gameMode;

        SetWorldPosition(worldPos);
    }

    
    void OnTriggerEnter(Collider other)
    {
        if (!consumed && other.GetComponent<Player>() != null)
        {
            //sound
            if(SoundEffects.instance)
                SoundEffects.instance.PlayClip(pickupSound);

            consumed = true;
            gameMode.PlayerConsumedPickup();
           
            Destroy(gameObject);
        }
    }
}
