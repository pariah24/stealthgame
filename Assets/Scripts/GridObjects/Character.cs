﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GridNavAgent))]
public class Character : GridObject
{
    public float movementSpeed = 1;
    public float rotationSpeed = 1;
    public bool canRotateWhileMoving = false;

    [HideInInspector]
    public bool isAlive;

    protected GridNavAgent gridNavAgent;
    protected GameMode gameMode;


    public virtual void Init(GameMode gMode)
    {
        isAlive = true;
        gameMode = gMode;
        gridNavAgent = GetComponent<GridNavAgent>();
        gridNavAgent.Init(gMode.gridManager.gridNav, movementSpeed, rotationSpeed, canRotateWhileMoving);
    }
}
