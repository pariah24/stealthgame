﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    public ParticleSystem cursorPressParticles;
    public AudioClip deathClip;

    [HideInInspector]
    public bool inputEnabled;

    Plane GroundPlane;
    List<Enemy> enemiesInLevel;
    Material matInstance;
    bool beingFollowed;
    ParticleSystem spawnedCursorParticles;

    public void Init(GameMode gMode, List<Enemy> enemiesInLevel)
    {
        base.Init(gMode);

        this.enemiesInLevel = enemiesInLevel;

        //plane to raycast against to get world position under cursor
        GroundPlane = new Plane(Vector3.up, Vector3.zero);

        //init distance map writer
        RenderDistanceMap distanceMapWrite = GetComponentInChildren<RenderDistanceMap>();
        distanceMapWrite.Init(gMode.mazeGenerationType.gridSize);

        //get material
        matInstance = GetComponent<MeshRenderer>().material;

        //instantiate particles ahead of time and reuse
        spawnedCursorParticles = Instantiate(cursorPressParticles); 
    }

	void Update()
    {
        if (!isAlive)
            return;

        //on mouse button, attempt move
        if (Input.GetMouseButtonDown(0) && inputEnabled)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float distance = 0;

            //raycast to get where in world space the user clicked, attempt to move there
            if (GroundPlane.Raycast(ray, out distance))
            {
                Vector3 hitPoint = ray.GetPoint(distance);

                GridObject.GridCoordinate spacePressed = new GridObject.GridCoordinate();

                if (gridNavAgent.RequestMove(hitPoint, out spacePressed))
                {
                    spawnedCursorParticles.transform.localPosition = spacePressed.worldPosition;
                    spawnedCursorParticles.Play();
                }
            }
        }

        //get highest enemy visibilty(i.e threat level)
        float highestVisibility = 0;
        foreach (var e in enemiesInLevel)
            highestVisibility = Mathf.Max(e.PlayerVisibilty, highestVisibility);

        //set color based off threat level
        matInstance.SetColor("_Color", Color.Lerp(Color.green, Color.red, highestVisibility));
    }

    public void Kill()
    {
        isAlive = false;
        GetComponent<Animator>().SetTrigger("Kill");

        //sound
        if (SoundEffects.instance)
            SoundEffects.instance.PlayClip(deathClip);
    }
}
