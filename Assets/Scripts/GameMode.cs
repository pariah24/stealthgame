﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMode : MonoBehaviour
{
    public Player playerPrefab;
    public Enemy enemyPrefab;
    public Pickup pickupPrefab;

    public MazeGenerationType mazeGenerationType;
    public Canvas uiCanvas;
    public GridManager gridManager;
    public AudioClip winSound;

    List<Enemy> enemiesInLevel = new List<Enemy>();
    List<Pickup> pickups = new List<Pickup>();
    Player player;
    int pickupsForExit;

    IEnumerator Start()
    {
        //allow gameobjects in scene to run Start before begining proper initilization of game
        yield return null;
        Init();

        while (!LevelMoveEvent.ascended)
            yield return null;

        player.inputEnabled = true;
    }

    

    void Init(VictoryPillar startPillar = null)
    {
        //only support odd sized grids for now
        if (mazeGenerationType.gridSize % 2 == 0)
        {
            Debug.LogError("Only odd sized grids are supported");
        }
        else
        {
            //remove old powerups
            foreach (var p in pickups)
                if(p != null)
                    Destroy(p.gameObject);
            pickups.Clear();

            //destroy any current enemies
            foreach (var e in enemiesInLevel)
                Destroy(e.gameObject);
            enemiesInLevel.Clear();

            //destroy grid content if any exist
            gridManager.DestroyGridContents(startPillar == null);

            //init grid
            MazeGenerationType.MazeLayout mazeLayout = mazeGenerationType.GenerateLayout(mazeGenerationType.gridSize, startPillar);
            gridManager.Init(mazeGenerationType.gridSize, mazeLayout, startPillar);

            //init player
            if (player == null)
                player = Instantiate(playerPrefab);
            gridManager.PlaceObjectOnSpace(player, gridManager.startPillar.offGrid.worldPosition);
            gridManager.endPillar.onPlayerEnter.AddListener(OnPlayeWon);

            //spawn enemies
            SpawnEnemies(mazeLayout.enemyPatrolPaths);

            //spawn pickups
            SpawnPickups(mazeLayout.pickupLocations);

            //init player and enemies
            player.Init(this, enemiesInLevel);
        }
    }

    IEnumerator RestartLevel(float delay = 0)
    {
        yield return new WaitForSeconds(delay);

        //fade and tield
        uiCanvas.GetComponentInChildren<Animator>().SetTrigger("FadeOut");
        while (FadeEvent.fadedIn)
            yield return null;

        //detroy player
        Destroy(player.gameObject);
        player = null;

        //reinit level
        Init();

        //fade out and yield
        uiCanvas.GetComponentInChildren<Animator>().SetTrigger("FadeIn");
        while (!FadeEvent.fadedIn)
            yield return null;

        player.inputEnabled = true;
    }

    IEnumerator NextLevel(float delay = 0)
    {
        player.inputEnabled = false;

        yield return new WaitForSeconds(delay);

        //reparent starting pillar so it moves down with level
        gridManager.startPillar.transform.SetParent(gridManager.transform);

        //move level offscreen
        gridManager.GetComponent<Animator>().SetTrigger("Down");
        while (LevelMoveEvent.ascended)
            yield return null;

        //reinit level
        gridManager.endPillar.onPlayerEnter.RemoveListener(OnPlayeWon);
        Init(gridManager.endPillar);

        //move level offscreen
        gridManager.GetComponent<Animator>().SetTrigger("Up");
        while (!LevelMoveEvent.ascended)
            yield return null;

        player.inputEnabled = true;
    }

    void SpawnEnemies(List<List<GridObject.GridCoordinate>> enemyPatrols)
    {
        for (int i = 0; i < enemyPatrols.Count; i++)
        {
            //create enemy
            Enemy enemy = Instantiate(enemyPrefab, gridManager.transform);

            List<Vector3> patrolPath = new List<Vector3>();
            for (int j = 0; j < enemyPatrols[i].Count; j++)
            {
                patrolPath.Add(gridManager.GridToWorld(enemyPatrols[i][j].x, enemyPatrols[i][j].y));
            }

            //place enemy on grid and init
            gridManager.PlaceObjectOnSpace(enemy, patrolPath[patrolPath.Count - 1]);
            enemy.Init(this, patrolPath, player);
            enemiesInLevel.Add(enemy);
        }
    }

    void SpawnPickups(List<GridObject.GridCoordinate> pickupLocations)
    {
        pickupsForExit = pickupLocations.Count;

        foreach (var l in pickupLocations)
        {
            Pickup p = Instantiate(pickupPrefab, gridManager.transform);
            p.Init(this, gridManager.GridToWorld(l.x, l.y));
            pickups.Add(p);
        }
    }

    public void KillPlayer()
    {
        //already killed
        if (!player || !player.isAlive)
            return;

        //play destroy animation
        player.Kill();

        StartCoroutine(RestartLevel(2));
    }


    public void PlayerConsumedPickup()
    {
        if(--pickupsForExit <= 0)
        {
            gridManager.endPillar.Activate();
        }
    }

    void OnPlayeWon()
    {
        //play sound
        if(SoundEffects.instance)
            SoundEffects.instance.PlayClipWithDelay(winSound, 0.5f);

        player.inputEnabled = false;
        StartCoroutine(NextLevel(1));
    }

    void Update()
    {
        //easy way to kill the game
        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
