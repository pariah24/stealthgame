﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeEvent : MonoBehaviour
{
    public static bool fadedIn;

    public void FadeDone(int fadeIn)
    {
        fadedIn = fadeIn == 1;
    }
}
