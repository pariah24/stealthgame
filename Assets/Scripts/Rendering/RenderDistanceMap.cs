﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderDistanceMap : MonoBehaviour
{
    public Shader distanceWriteShader;
    public RenderTexture distanceMap;

    bool initilized;
    Camera distanceWriteCam;


    public void Init(float mapSize)
    {
        //get camera and change shader to only write value based on distance to player
        distanceWriteCam = GetComponent<Camera>();
        distanceWriteCam.SetReplacementShader(distanceWriteShader, "");

        Shader.SetGlobalFloat("_MaxWorldDist", mapSize * 1.5f);
        initilized = true;
    }

	void LateUpdate ()
    {
        if (!initilized)
            return;

        Shader.SetGlobalVector("_PlayerPosition", transform.position);
        distanceWriteCam.RenderToCubemap(distanceMap);      
    }
}
