﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class VictoryPillar : MonoBehaviour
{
    [HideInInspector]
    public UnityEvent onPlayerEnter;

    public bool isActivated { get; private set; }
    public Vector3 activateOffset;
    public AnimationCurve activationCurve;
    public float activationDuration = 1;
    [Range(0, 1)]
    public float playSoundAtHeight = 1;
    public AudioClip pillarRaiseSound;

    public GridObject.GridCoordinate onGrid; // space adjacent to victory tile
    public GridObject.GridCoordinate offGrid; // space the tile is actually on which is not actuall on the grid

    bool isEndGoal;
    bool isActivating;
    bool hasPlayedRaiseSound;
    float activationTime;
    

    public void Init(bool isEndGoal, GridObject.GridCoordinate onGrid, GridObject.GridCoordinate offGrid)
    {
        this.onGrid = onGrid;
        this.offGrid = offGrid;
        this.isEndGoal = isEndGoal;
        isActivated = !isEndGoal;
        hasPlayedRaiseSound = false;

        if (isEndGoal)
            transform.localPosition = transform.localPosition - activateOffset;
    }

    void Update()
    {
        if(!isActivated && isActivating)
        {
            activationTime += Time.deltaTime;
            transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z) - new Vector3(0, activateOffset.y * activationCurve.Evaluate( 1 - (activationTime / activationDuration)), 0);

            if (!hasPlayedRaiseSound && activationTime >= playSoundAtHeight)
            {
                hasPlayedRaiseSound = true;

                //play sound
                if (SoundEffects.instance)
                    SoundEffects.instance.PlayClip(pillarRaiseSound);
            }

            if (activationTime >= activationDuration)
            {
                isActivated = true;
            }
        }
    }

    public void Activate()
    {
        isActivating = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (isActivated && isEndGoal && other.GetComponent<Player>() != null)
        {
            onPlayerEnter.Invoke();
        }
    }
}
