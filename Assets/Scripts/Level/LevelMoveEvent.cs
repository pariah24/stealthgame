﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMoveEvent : MonoBehaviour
{
    public static bool ascended;

    public void FinishedMovement(int didAscend)
    {
        ascended = didAscend == 1;
    }
}
