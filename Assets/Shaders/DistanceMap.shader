﻿Shader "Custom/DistanceMap" 
{
	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		
		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "PlayerVisibilityCull.cginc"

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float3 worldPos : TEXCOORD0;
			};

			v2f vert(appdata_base v) 
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}

			half4 frag(v2f i) : SV_Target
			{
				return half4(DistanceToPlayer(i.worldPos.xyz), 0, 0, 1);
			}

			ENDCG
		}
	}
}
