﻿

Shader "Custom/CullByPlayerSight" 
{
	Properties
	{
		_lit("lit", Int) = 1
		_OnlyRemoveLighting("LoS unlight", Int) = 1 //when out of players sight do not light. otherwise it is culled completely
		_DistanceMap ("player shadowmap", CUBE) = "" {}
		_Color("color", Color) = (1,1,1,1)
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"}
		LOD 200

		// Main pass for ambient lighting
		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#pragma multi_compile_fwdbase nolightmap nodynlightmap novertexlight
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			#include "PlayerVisibilityCull.cginc"

			samplerCUBE _DistanceMap;
			fixed4 _Color;
			int _OnlyRemoveLighting;

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 worldPos : TEXCOORD0;
				//SHADOW_COORDS(1) // shadow parameters to pass from vertex
			};

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				//TRANSFER_SHADOW(o); // pass shadow coordinates to pixel shader
				return o;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				// no need for attenuation as there are no direction lights in scene
				//UNITY_LIGHT_ATTENUATION(attn, IN, 0)
				//fixed4 c = _LightColor0.rgb * attn;

				fixed4 c = 0.1;
				c *= _Color;

				//if out of line of sight, either cull or stay unlit
				half los = LineOfSight(IN.worldPos, _DistanceMap);
				if (los == 0 && _OnlyRemoveLighting == 0)
					discard;
				else
					c += AMBIENT_WORLD_COLOR;

				return c;
			}

			ENDCG

		}	

		// ForwardAdd pass for spot lights
		Pass
		{
			Tags{ "LightMode" = "ForwardAdd" }
			//Blend SrcAlpha OneMinusSrcAlpha
			Blend One One
			//Blend OneMinusDstColor One
			//Blend One OneMinusSrcAlpha


			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// Include shadowing support for point/spot
			#pragma multi_compile_fwdadd_fullshadows
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			#include "PlayerVisibilityCull.cginc"

			samplerCUBE _DistanceMap;
			fixed4 _Color;
			int _OnlyRemoveLighting;
			int _lit;

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 worldPos : TEXCOORD1;
				SHADOW_COORDS(0)
			};

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				TRANSFER_SHADOW(o); // pass shadow coordinates to pixel shader
				return o;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				UNITY_LIGHT_ATTENUATION(atten, IN, IN.worldPos)
				fixed4 c = atten * _lit;

				c *= _Color;

				//if out of line of sight, either cull or stay unlit
				half los = LineOfSight(IN.worldPos, _DistanceMap);
				if (los == 0 && _OnlyRemoveLighting == 0)
					discard;
				else if (los == 1)
					c.rgb *= _LightColor0.rgb;
				else
					c = 0;

				return c;
			}

			ENDCG
		}

		//cast shadows
		UsePass "VertexLit/SHADOWCASTER"
	}
}