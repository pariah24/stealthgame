﻿Shader "Custom/LowerGroundFade"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_HeightFalloff("Height fade falloff", Float) = -10
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		//ZWrite Off
		//Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD0;
			};
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}

			fixed4 _Color;
			float _HeightFalloff;
			
			fixed4 frag (v2f i) : SV_Target
			{
				//base color
				fixed4 col = _Color;
			
				//apply fade based of world height
				col *= clamp(1 - (i.worldPos.y/ _HeightFalloff), 0, 1);
				return col;
			}
			ENDCG
		}
	}
}
