#ifndef VISBILITYCULL_INCLUDED
#define VISBILITYCULL_INCLUDED

#define AMBIENT_WORLD_COLOR fixed4(0.2, 0.2, 0.2, 1);

float4 _PlayerPosition; //current position of the player
float _MaxWorldDist; // max distance an object can possibly be from the player

half DistanceToPlayer(float3 worldPos)
{
	//0 is close to player, 1 is furthest possible distance to player
	return length(_PlayerPosition.xyz - worldPos) / _MaxWorldDist;
}

fixed LineOfSight(float3 worldPos, samplerCUBE distanceMap)
{
	//vector from player to world position
	worldPos.y += 0.05; // this is prevent accuracy problems particularily with the floor
	float3 cubeProjDirection = worldPos - _PlayerPosition.xyz;
	
	//get distance value from cubemap
	float4 sample = texCUBE(distanceMap, normalize(cubeProjDirection));
	//if our distance is further then player visibility distance in this direction, we are not visible to player
	if(DistanceToPlayer(worldPos) >= sample.r + 0.0015)
		return 0;
	else
		return 1;
}

#endif